from django.contrib.auth import login
from django.shortcuts import redirect, render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends':friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}

    if request.method == 'POST':
        form = FriendForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('/lab-3')
    else:
        form = FriendForm()

    return render(request, "lab3_form.html",{'form': form})
